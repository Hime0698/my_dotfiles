# My_Bashrc

My personalised Dotfiles backup.

I am sure this is not perfect and some of my additions are kinda unnecessary and flashy.
If you are trying to run it for the first time you will get a bunch of errors; to solve this, run initialinstall to install all necessary programs and repositories.
Please note theat this is built for debian based systems and includes the default mods done on Ubuntu.

The section where I added my personal edits is at the end and marked, it takes up a lot more space than it could because I have spaced everything out and added a
bunch of comments to make everything really easy to read and understand for people who are new to .bashrc file editing and/or linux in general.

NOTE: This was originally in a different repo and I migrated it to this one so the name of the repo better reflected its contents.
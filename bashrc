# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi




######################################################################
#                                                                    #
# My mods start here                                                 #
#                                                                    #
######################################################################


     ##########################################################
     #                                                        #
     #  Distro Specific Mods                                  #
     #                                                        #
     ##########################################################

# This will automattically check for and install updates as well as remove outdated stuff.
alias update='sudo apt update; sudo apt upgrade; sudo apt dist-upgrade; sudo apt autoremove; sudo apt autoclean'

# Installs all the things needed for the base .bashrc settings to work.
alias initialinstall='install neofetch bat htop figlet lolcat curl ripgrep cargo -y;
        cargo install -f --git https://github.com/cjbassi/ytop ytop;
        install ddate tmux ttf-mscorefonts-installer -y; cargo install lsd;
		git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1
		clear; git clone https://github.com/itchyny/lightline.vim ~/.vim/pack/plugins/start/lightline; clear; reloadbashrc'

# Installs some of my most frequently used command line applications.
alias DefaultAppsCmd='install ffmpeg youtube-dl build-essential vim 2fsprogs aria2 -y'

# Installs some of my most frequently used GUI applications.
alias DefaultAppsGui='sudo apt-add-repository ppa:qbittorrent-team/qbittorrent-stable;
        install qbittorrent -y; sudo add-apt-repository ppa:obsproject/obs-studio; install obs-studio -y;
        install vlc bleachbit gimp -y;'

# This is a simple shortcut to save typing when installing stuff in the terminal.
alias install='sudo apt install'

# updates and overwrites the .bashrc file to my latest version
# please not in order for this to work you must first clone this repository into your Documents folder and NOT rename it
alias BrcUp='cd; cd Documents/my_dotfiles; git pull; sudo cp bashrc ~/.bashrc; cd ~; reloadbashrc'

# Install zsh
alias installZsh='install zsh; sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"'


     ##########################################################
     #                                                        #
     #  Distro Independent Mods                               #
     #                                                        #
     ##########################################################

# Show system info on terminal startup.
neofetch --config ~/.config/neofetch/config.conf

# Date passed into lolcat for the rainbow effect.
date

# Discordian date piped into lolcat for the rainbow effect.
ddate

# Displays the number of days since the Unix Epoch with the number in red.
RED='\033[0;31m'; NC='\033[0m'; echo -e 'It has been' ${RED}$(($(date +%s) / 86400))${NC} 'days since the Unix Epoch (00:00:00 UTC, Thursday, Jan 1st, 1970)'

# Dislays the message Welcome Jacob! in ascii charecters on terminal startup.
figlet Welcome $USER! | lolcat

# Run all install scripts
alias installAll='initialinstall; DefaultAppsCmd; DefaultAppsGui; reloadbashrc'

# Runs Gotop instead of top because I like it better.
alias top='gotop'

# Lets me easilly reload the .bashrc file without restarting the terminal after changes.
alias reloadbashrc='clear; source ~/.bashrc'

# This is a forkbomb, running this command will freeze your computer and force you to reboot
#      to be honest I am not 100% sure why I made this, I was just bored.
alias DieSucker='echo "See YA!" && :(){ :|: & };:'

# This is just a quick shortcut to save time when trying to make things executable.
alias mkx='sudo chmod +x'

# Quickly adds all changes and commits them to master, not the best git practice
#       but it works to quickly backup a whole folder for personal use.
alias GitUp='git add -A; git commit -a; git push'

# Quickly trims the SSD as I use one as my boot drive and trimming will speed it up a lot.
alias trim='sudo fstrim -v /'

# Updates my neofetch config file from this repo
alias NeoUp='cd ~; cd Documents/my_dotfiles; git pull; cp Neofetch_Config ~/.config/neofetch/config.conf; cd ~; reloadbashrc'

# Updates my .vimrc from this repo
alias VrcUp='cd ~; cd Documents/my_dotfiles; git pull; sudo cp Vim/Vimrc /etc/vimrc; sudo cp Vim/Vimrc ~/.vimrc; cd ~; reloadbashrc'

# Defragments Hard Drive
alias Defragment='sudo e4defrag  /'

# Changes grep to RipGrep for faster grepping.
alias grep='rg -p'

# Lists free disk space in an easy to read format (Credit: Dr. Gourd)
alias df='df -Th'

# Displays histoty ordered by frequency of use (Credit: Dr. Gourd)
alias h2='history | awk '"'"'{ CMD[$2]++; count++; } END { for (a in CMD) print CMD[a] " " CMD[a] / count * 100 "% " a; }'"'"'| grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n25'

# A nice easy to read variation on ls (Credit: Dr. Gourd)
alias lss='ls -Alh $*'

# Safe ReMove instead of deleting a file or directory this command moves it to the trashcan
alias srm='mv -f -t $HOME/.local/share/Trash/files $*'

# Use the bash-git-prompt by migicmonty
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
fi

# Automatically exctract archives. (Credit: Jeff Williams)
# Note: I Need to fix this to allow spaces in file names
extract () {
    for archive; do
            if [ -f $archive ] ; then
                    case $archive in
                            *.tar.bz2)   tar xvjf $archive    ;;
                            *.tar.gz)    tar xvzf $archive    ;;
                            *.bz2)       bunzip2 $archive     ;;
                            *.rar)       rar x $archive       ;;
                            *.gz)        gunzip $archive      ;;
                            *.tar)       tar xvf $archive     ;;
                            *.tbz2)      tar xvjf $archive    ;;
                            *.tgz)       tar xvzf $archive    ;;
                            *.zip)       unzip $archive       ;;
                            *.Z)         uncompress $archive  ;;
                            *.7z)        7z x $archive        ;;
                            *)           echo "don't know how to extract '$archive'..." ;;
                    esac
            else
                    echo "'$archive' is not a valid file!"
            fi
    done
}

# Changes out cat for Bat a fancier rust reimplementation
alias cat='bat'

#Changes ls to lsd to use lsd which is a prettier ls with colors and icons written in rust
alias ls='lsd'
alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'


# Other settings to remember:
#   Change default terminal size to 95x34
#   Change background color to black
#   Change cursor color to #2EEB27

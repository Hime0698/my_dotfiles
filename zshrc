# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git
	colored-man-pages
	cp
	extract
	zsh-syntax-highlighting
    zsh-autosuggestions)
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

######################################################################
#                                                                    #
# My mods start here                                                 #
#                                                                    #
######################################################################


     ##########################################################
     #                                                        #
     #  Distro Specific Mods                                  #
     #                                                        #
     ##########################################################

# This will automattically check for and install updates as well as remove outdated stuff.
alias update='sudo apt update; sudo apt full-upgrade; sudo apt dist-upgrade; sudo apt autoremove; sudo apt autoclean'

# Installs all the things needed for the base .zrc settings to work.
alias initialinstall='install neofetch htop figlet lolcat git curl fd-find bat ripgrep cargo ddate tmux ttf-mscorefonts-installer -y;
        export PATH=~/.cargo/bin:$PATH;
		cargo install -f --git https://github.com/cjbassi/ytop ytop; 
		cargo install --git https://github.com/Peltoche/lsd.git --branch master;
	mkdir -p ~/.local/share/fonts; cd ~/.local/share/fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf;
        cd; sudo git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k;
        cp Documents/my_dotfiles/p10k.zsh ~/.p10k.zsh;
        cd; git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions;
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting;
        cd Documents/my_dotfiles; git pull; sudo cp zshrc.dotfile ~/.zshrc;
        cd ~; chsh -s /bin/zsh; git clone https://github.com/itchyny/lightline.vim ~/.vim/pack/plugins/start/lightline'

# Installs some of my most frequently used command line applications.
alias DefaultAppsCmd='install ffmpeg -y; install youtube-dl -y;
        install build-essential -y; install vim -y; install e2fsprogs; install aria2'

# Installs some of my most frequently used GUI applications.
alias DefaultAppsGui='sudo apt-add-repository ppa:qbittorrent-team/qbittorrent-stable;
        install qbittorrent -y; sudo add-apt-repository ppa:obsproject/obs-studio; install obs-studio -y;
        install vlc -y; install bleachbit -y; install gimp -y;'

# This is a simple shortcut to save typing when installing stuff in the terminal.
alias install='sudo apt install'

# updates and overwrites the .zshrc file to my latest version
# please not in order for this to work you must first clone this repository into your Documents folder and NOT rename it
alias ZrcUp='cd; cd Documents/my_dotfiles; git pull; sudo cp zshrc ~/.zshrc; cd ~; reloadzrc'


     ##########################################################
     #                                                        #
     #  Distro Independent Mods                               #
     #                                                        #
     ##########################################################

# Add Cargo to path
export PATH=~/.cargo/bin:$PATH

# Show system info on terminal startup.
neofetch --config ~/.config/neofetch/config.conf

# Date passed into lolcat for the rainbow effect.
date

# Discordian date piped into lolcat for the rainbow effect.
ddate

# Displays the number of days since the Unix Epoch with the number in red.
RED='\033[0;31m'; NC='\033[0m'; echo -e 'It has been' ${RED}$(($(date +%s) / 86400))${NC} 'days since the Unix Epoch (00:00:00 UTC, Thursday, Jan 1st, 1970)'

# Dislays the message Welcome Jacob! in ascii charecters on terminal startup.
figlet Welcome $USER! | lolcat

# Run all install scripts
alias installAll='initialinstall; DefaultAppsCmd; DefaultAppsGui; reloadzrc'

# Runs Ytop instead of top because I like it better.
alias top='ytop'

# Lets me easilly reload the .zshrc file without restarting the terminal after changes.
alias reloadzrc='clear; source ~/.zshrc'

# This is a forkbomb, running this command will freeze your computer and force you to reboot
#      to be honest I am not 100% sure why I made this, I was just bored.
alias DieSucker='echo "See YA!" && :(){ :|: & };:'

# This is just a quick shortcut to save time when trying to make things executable.
alias mkx='sudo chmod +x'

# Quickly adds all changes and commits them to master, not the best git practice
#       but it works to quickly backup a whole folder for personal use.
alias GitUp='git add -A; git commit -a; git push'

# Quickly trims the SSD as I use one as my boot drive and trimming will speed it up a lot.
alias trim='sudo fstrim -v /'

# Updates my neofetch config file from this repo
alias NeoUp='cd ~; cd Documents/my_dotfiles; git pull; cp Neofetch_Config ~/.config/neofetch/config.conf; cd ~; reloadzrc'

# Updates my .vimrc from this repo
alias VrcUp='cd ~; cd Documents/my_dotfiles; git pull; sudo cp Vim/Vimrc.dotfile /etc/vimrc; sudo cp Vim/Vimrc.dotfile ~/.vimrc; cd ~; reloadzrc'

# Defragments Hard Drive
alias Defragment='sudo e4defrag  /'

# Changes grep to RipGrep for faster grepping.
alias grep='rg'

# Lists free disk space in an easy to read format (Credit: Dr. Gourd)
alias df='df -Th'

# Displays histoty ordered by frequency of use (Credit: Dr. Gourd)
alias h2='history | awk '"'"'{ CMD[$2]++; count++; } END { for (a in CMD) print CMD[a] " " CMD[a] / count * 100 "% " a; }'"'"'| grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n25'

# A nice easy to read variation on ls (Credit: Dr. Gourd)
alias lss='ls -Alh $*'

# Safe ReMove instead of deleting a file or directory this command moves it to the trashcan
alias srm='mv -f -t $HOME/.local/share/Trash/files $*'

# Changes out cat for Bat a fancier rust reimplementation
alias cat='bat'

#Changes ls to lsd to use lsd which is a prettier ls with colors and icons written in rust
alias ls='lsd'
alias l='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lt='ls --tree'

# Change find to fd a faster rust alternative
alias find='fd'

# Other settings to remember:
#   Change default terminal size to 95x34
#   Change background color to black
#   Change cursor color to #2EEB27

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
